package guy.droid.com.payumoneydroid;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;

import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class MainActivity extends AppCompatActivity {
EditText fname,amount,email,phone;
Button button;
    Double damount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fname = (EditText)findViewById(R.id.firstname);
        amount = (EditText)findViewById(R.id.amount);
        email =  (EditText)findViewById(R.id.email);
        phone = (EditText)findViewById(R.id.phone);
        button = (Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* Bundle bundle = new Bundle();
                bundle.putString("fname",fname.getText().toString());
                bundle.putString("email",email.getText().toString());
                bundle.putString("phone", phone.getText().toString());
                bundle.putDouble("amount", amounts); */

                Intent intent = new Intent(getApplicationContext(),Payments.class);
                intent.putExtra("fname",fname.getText().toString());
                intent.putExtra("email", email.getText().toString());
                intent.putExtra("phone", phone.getText().toString());
                intent.putExtra("amount",amount.getText().toString());
                startActivity(intent);
            }
        });

    }
}
